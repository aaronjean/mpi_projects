#include <mpi.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "computePi.hpp"


int howManyTimesShouldIRun(int my_rank, int world_size, int n) {

    int x;

    x = n/(world_size);

    if(n%(world_size) >= my_rank) {
        x++;
    }

    return x;

}



int main(int argc, char* argv[]) {

    int aPD = std::stoi(argv[1]);

    std::vector<int> piVec;

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    world_size -=1;

    // Get the rank of the process
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    
    int digit;
    int count = my_rank;

    if(my_rank != 0){
    
    int indivNodeRunAmount = howManyTimesShouldIRun(my_rank, world_size, aPD);

        for (int i = 0; i < indivNodeRunAmount; ++i) {

            digit = computePiDigit(count);
            MPI_Send(&digit, 1, MPI_INT, 0, i, MPI_COMM_WORLD);
            count += (world_size);

        }

    }else {

        std::cout << "3.";
        int minRunAmount = aPD/(world_size);

        for (int j = 0; j < minRunAmount; ++j) {

            for (int i = 1; i <= (world_size); ++i) {

                MPI_Recv(&digit, 1, MPI_INT, i, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                std::cout << digit;

            }
        }

        int addRunAmount = aPD%(world_size);

        for (int k = 1; k <= addRunAmount; ++k) {

            MPI_Recv(&digit, 1, MPI_INT, k, minRunAmount, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            std::cout << digit;
        }
        std::cout << std::endl;

    }

    // Finalize the MPI environment.
    MPI_Finalize();
}